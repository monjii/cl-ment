///////////////////////////////////////////////////////////////////////////////
//
//		Réalises un programme qui affiche l'aphabet dans l'ordre puis un retour
//		à la ligne
//
//		Rappel : on peut écrire char c = 'a';
//
//
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

void	print_alphabet()
{
	char c = '0';
	while (c <= '9'){
		printf("%c", c);
		c++;
	}
	printf("\n");
}

int		main()
{
	print_alphabet();
	return (0);
}