#include <stdlib.h>
#include <stdio.h>
#include <time.h>

///////////////////////////////////////////////////////////////////////////////
//
//	Fonction :	get_user_choice()
//
//	Rôle :		récupérer le choix utilisateur à l'aide de scanf(), puis return
//				ce choix; En cas de choix erroné, message d'erreur puis exit()
//
///////////////////////////////////////////////////////////////////////////////
#define CISEAU 0
#define PIERRE 1
#define FEUILLE 2

int		get_user_choice()
{
	int choix;

	scanf("%d", &choix);
	if (choix != CISEAU && choix != PIERRE && choix != FEUILLE)
	{
		printf("Mauvais choix, idiot\n");
		exit (0);
	}
	return (choix);
}

int		main(void)
{
	srand(time(NULL));
	int		choix_ordi;
	int		choix_joueur;
	char OK = 1;
	char qqq; // Bon je sais pas faire un string et pis du coup j'y arrive plus et j'ai hâte demain, il faudra que je note aussi :)


while (OK == 1){

	choix_ordi = rand() % 3;
	printf("Chère Jessye veuillez entrer 0 pour CISEAU, 1 pour PIERRE, 2 pour FEUILLE : \n");
	choix_joueur = get_user_choice();

	if (choix_joueur == CISEAU && choix_ordi == FEUILLE){

		printf("Choix ordi : %i\n ", choix_ordi);
		printf("Super Jessye ! Ton choix du CISEAU l'ors de ce combat était une super idée car le CISEAU bat la FEUILLE\n");
	}

	else if (choix_joueur  == CISEAU && choix_ordi == PIERRE ){
		printf("Choix ordi : %i\n ", choix_ordi);
		printf("Ho non Jessye :( Qu'est-ce qui t'as fait penser ça ? Tu as perdu car le CISEAU perd VS la PIERRE\n");
	}

	else if (choix_joueur == FEUILLE && choix_ordi == CISEAU){
		printf("Choix ordi : %i\n ", choix_ordi);
		printf("Holala... Concentre toi mieux si tu veux passer le test du grand dresseur POKÉMON ! Tu as Perdu car la FEUILLE perd VS le CISEAU\n");
	}

	else if (choix_joueur == FEUILLE && choix_ordi == PIERRE){

		printf("Choix ordi : %i\n ", choix_ordi);
		printf("YES ! Super Jessye ! La force te gagne car tu as sentie qu'il fallait jouer la FEUILLE qui gagne VS la PIERRE\n");
	}

	else if (choix_joueur == PIERRE && choix_ordi == CISEAU){
		printf("Choix ordi : %i\n ", choix_ordi);
		printf("Sacripant ! Mais je n'ai jamais vu une dame jouée d'la sorte ! Ta PIERRE gagne VS le CISEAU\n");
	}

	else if (choix_joueur == PIERRE && choix_ordi == FEUILLE){
		printf("Choix ordi : %i\n ", choix_ordi);
		printf("HAHAHA L'ordi à lu dans tes pensées Jessye ! Et tu as perdu car la PIERRE perd VS la FEUILLE\n");
	}

	else
		printf("EGALITÉ recommence !\n");
}
	// Ici construire les conditions, par ex : si (choix_ordi == pierre ET choix_joueur == ciseaux )
	//											printf("Pierre gagne ! VOous avez perdu");

	return (0);
}
