///////////////////////////////////////////////////////////////////////////////
//
//		Réaliser une boucle
//		Demander à l'utilisateur de saisir une valeur tant que cette meme
//		valeur saisie n'est pas 10
//
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#define OK	1

int		main()
{
	int	nombre_a_trouver = 4000;
	int d;
	char c;

	c = OK;

	while (c == OK){

		scanf("%d", &d);

		if (d > nombre_a_trouver)
			printf("C'est moins !\n");

		else if (d < nombre_a_trouver)
			printf("C'est plus !\n");

		else {
			printf("BRAVO !!!!\n");
			c = !OK;

			}

	}


	return (0);
}
