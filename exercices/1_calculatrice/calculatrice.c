///////////////////////////////////////////////////////////////////////////////
//////////////////////////////////CALCULATRICE/////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//
//	Écris un programme qui :
//		- écrit "Calculatrice" et saute 2 lignes
//		- écrit "Valeur de a :" et saute 1 ligne
//		- récupère la valeur de a à l'aide de scanf()*
//		- écrit "Valeur de b :" et saute 1 ligne
//		- récupère la valeur de b à l'aide de scanf()*
//		- écrit "Valeur de a+b : " puis la valeur de la somme des deux variables
//
//	* scanf est une fonction qui permet de stocker une valeur à partir du term
//	  et de la stocker dans une variable; dans le cas présent, il faut écrire :
//
//		int lol = 0;
//								Le & devant lol signifie qu'on va écrire dans 
//		scanf("%d", &lol);		la variable lol; &lol signifie "adresse mémoire
//								de lol". A voir plus tard
//								
//		Le %d indique juste a la fonction que l'on veut récupérer un integer
//
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>

int		main()
{
	int a;

	printf("Calculatrice\n\n");
	printf("Valeur de a :\n");
	scanf("%i", &a);
	printf("Valeur de b :\n");

	int b;

	scanf("%i", &b);

	printf("Valeur de a+b :%i\n", a + b);

	return (0);
}