///////////////////////////////////////////////////////////////////////////////
//
//	Écris un programme qui saisit un nombre puis indique à
//	l’utilisateur si ce nombre est plus grand ou plus petit qu’un autre
//	nombre défini à l’avance dans le programme.
//
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

int		main()
{
	int	lol = 100;

	int d;

	scanf("%d", &d);

	if (d > lol)
	{
		printf("plus grand\n");
	}

	else if (d < lol)
	{
		printf("plus petit\n");
	}

	else{
		printf("égal\n");
	}


	return (0);
}
