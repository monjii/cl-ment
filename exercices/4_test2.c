#include <unistd.h>

char	*copie_tableau(char tab[])
{
	char tab2[150];				//150 pour avoir la place disponible pour la copie
	int i = 0;

	while (tab[i] != '\0'){
		tab2[i] = tab[i];		// Boucle de copie
		i++;
	}
	tab2[i] = tab[i];			//caractere final '\0'

	return(tab2);
}

int		main()
{
	char tab[] = "Copie moi stp stp stp stp stp je me sens si seul merci Clément stp salut";

	printf("%s", copie_tableau(tab));

	return (0);
}