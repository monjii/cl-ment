int		main()
{
	char	*tab;

	printf("Adresse pointeur %p\n", &tab);
	printf("Adresse debut hello %p\n", tab);
	printf("Adresse debut hello (&tab[0]) %p\n", &tab[0]);

	printf("Contenu pointeur %c\n", tab[0]);
	printf("Contenu pointeur %c\n", *tab);



	// printf("%p\n", &a);

	// function(&a);
	// printf("%i\n", a);

	return (0);
}