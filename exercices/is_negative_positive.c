///////////////////////////////////////////////////////////////////////////////
//
//	Écris un programme qui :
//		- récupère une valeur entrée par l'utilisateur à l'aide de scanf()*
//			(scanf("%d", &ma_variable))
//		- Check le signe de cette valeur : si elle est négative, affiche "Negatif",
//		  si elle est positive, affiche "Positif", et si elle vaut 0, affiche 
//		  "Nul" suivi d'un retour a la ligne
//
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

int		main()
{	
	int d;

	scanf("%d",&d);

	if (d > 0){
		printf("Positif\n");
	}

	if (d < 0){
		printf("Negatif\n");
	}

	else if (d == 0){
		printf("Nul\n");
	}

	return (0);
}

