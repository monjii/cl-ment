///////////////////////////////////////////////////////////////////////////////
//
//		Réalises un petit jeu qui :
//		- Initialise un nombre aléatoire entre 0 et 99
//		- Tente de faire deviner ce nombre à l’utilisateur en lui indiquant si
//		le nombre à trouver est plus petit ou plus grand que sa proposition.
//
//
//		Entrez votre nombre: 50
//		C’est plus!
//		Entrez votre nombre: 25
//		C’est moins!
//		...
//		Gagné!!!
//
//
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define OK 1

int		main()
{
	srand(time(NULL));

	int	nombre_a_trouver = rand() % 5000;
	int d;
	char c;

	c = OK;

	while (c == OK){

		scanf("%d", &d);

		if (d > nombre_a_trouver)
			printf("C'est moins !\n");

		else if (d < nombre_a_trouver)
			printf("C'est plus !\n");

		else {
			printf("BRAVO !!!!\n");
			c = !OK;
		}

	}
	return (0);
}
